FROM python:3.6-alpine

LABEL maintainer "benjamin.bertrand@esss.se"


ENV REQUESTS_VERSION 2.19.1

RUN pip install "requests==${REQUESTS_VERSION}"
