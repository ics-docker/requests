requests docker image
=====================

Minimal Python docker image with [requests](http://docs.python-requests.org/en/master/) included.

Based on the official [python alpine docker image](https://hub.docker.com/_/python/).

Docker pull command:

```
docker pull registry.esss.lu.se/ics-docker/requests:latest
```
